#pragma once
#include <iostream>

using namespace std;

void Num(int N)
{
    cout << "0 ";
    for (int i = ((N % 2 == 0) ? 2 : 1); i <= N; i+= 2)
    {
        cout << i << ' ';
    }
}